CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides an integration for the [HaynesPro API](https://www.haynespro.co.uk).

 * For the description of the module visit: https://www.drupal.org/project/haynespro

 * To submit bug reports and feature suggestions, or to track changes visit: https://www.drupal.org/project/haynespro


REQUIREMENTS
------------

This module requires the following module:

* [Key](https://www.drupal.org/project/key)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

 * Configure new key in Configuration » System » Keys - Add Key (/admin/config/system/keys)
   * Type settings > Key type - User/password
   * Set up the rest as you prefer, Value - must contain `username` and `password`
 * Configure settings in Configuration » Web services » HaynesPro Settings (/admin/config/services/haynespro)


MAINTAINERS
-----------

 * Cezary Walas - https://www.drupal.org/u/cezary-walas
 * Zoltan Balint - https://www.drupal.org/u/zoltanb, https://zbalint.tech

Supporting organization:

 * Infopro Digital - https://www.drupal.org/infopro-digital