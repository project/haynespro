<?php

namespace Drupal\haynespro\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SettingsForm class.
 */
class SettingsForm extends ConfigFormBase {

  protected const CONFIG_NAME = 'haynespro.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'haynespro_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      self::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG_NAME);

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#default_value' => $config->get('base_url'),
      '#required' => TRUE,
    ];

    $form['key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Key'),
      '#default_value' => $config->get('key'),
      '#required' => TRUE,
      '#key_filters' => ['type' => 'user_password'],
    ];

    $form['version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#default_value' => $config->get('version'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(self::CONFIG_NAME)
      ->set('base_url', $form_state->getValue('base_url'))
      ->set('key', $form_state->getValue('key'))
      ->set('version', $form_state->getValue('version'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
