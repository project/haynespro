<?php

namespace Drupal\haynespro\Services;

interface HaynesProApiInterface {

  /**
   * Get vehicle by Vehicle Registration Mark (VRM).
   *
   * @param string $vehicleRegistrationMark
   *   Vehicle Registration Mark.
   * @param string $clientRef
   *   Client reference.
   * @param string $clientDescription
   *   Client description.
   *
   * @return \SimpleXMLElement|null
   *   XML or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getVehicleByVehicleRegistrationMark(string $vehicleRegistrationMark, string $clientRef, string $clientDescription): ?\SimpleXMLElement;

  /**
   * Get vehicle by Vehicle Identification Number (VIN).
   *
   * @param string $vehicleIdentificationNumber
   *   Vehicle Identification Number.
   * @param string $clientRef
   *   Client reference.
   * @param string $clientDescription
   *   Client description.
   *
   * @return \SimpleXMLElement|null
   *   XML or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getVehicleByVehicleIdentificationNumber(string $vehicleIdentificationNumber, string $clientRef, string $clientDescription): ?\SimpleXMLElement;

}
