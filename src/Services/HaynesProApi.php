<?php

namespace Drupal\haynespro\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * HaynesPro API class.
 */
class HaynesProApi implements HaynesProApiInterface {

  /**
   * Status code from the API: 1000 ("No vehicles returned").
   */
  protected const STATUS_CODE_NO_VEHICLES_RETURNED = 1000;

  /**
   * Base endpoint for getting vehicle information.
   */
  protected const ENDPOINT_GET_VEHICLE = '/Vehicle/v1/GetVehicle.asmx/';

  /**
   * API Keys.
   *
   * @var array $apiKeys
   */
  protected array $apiKeys;

  /**
   * API settings config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig $apiSettings
   */
  protected ImmutableConfig $apiSettings;

  /**
   * The HTTP Client.
   *
   * @var \GuzzleHttp\ClientInterface $httpClient
   */
  protected ClientInterface $httpClient;

  /**
   * The Logger service.
   *
   * @var \Psr\Log\LoggerInterface|\Drupal\Core\Logger\LoggerChannelInterface $logger
   */
  protected LoggerInterface $logger;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   The key repository.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ClientInterface $httpClient,
    KeyRepositoryInterface $keyRepository,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->apiSettings = $configFactory->get('haynespro.settings');
    $this->apiKeys = $keyRepository->getKey($this->apiSettings->get('key'))->getKeyValues();
    $this->httpClient = $httpClient;
    $this->logger = $loggerChannelFactory->get('haynespro');
  }

  /**
   * {@inheritdoc}
   */
  public function getVehicleByVehicleRegistrationMark(string $vehicleRegistrationMark, string $clientRef, string $clientDescription): ?\SimpleXMLElement {
    $uri = self::ENDPOINT_GET_VEHICLE . 'strB2BGetVehicleByVRM';
    $options['form_params']['strVRM'] = $vehicleRegistrationMark;
    $options['form_params']['strClientRef'] = $clientRef;
    $options['form_params']['strClientDescription'] = $clientDescription;

    return $this->apiCall($uri, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getVehicleByVehicleIdentificationNumber(string $vehicleIdentificationNumber, string $clientRef, string $clientDescription): ?\SimpleXMLElement {
    $uri = self::ENDPOINT_GET_VEHICLE . 'strB2BGetVehicleByVIN';
    $options['form_params']['strVIN'] = $vehicleIdentificationNumber;
    $options['form_params']['strClientRef'] = $clientRef;
    $options['form_params']['strClientDescription'] = $clientDescription;

    return $this->apiCall($uri, $options);
  }

  /**
   * API call.
   *
   * @param string $uri
   *   URI of the endpoint.
   * @param array $options
   *   Request options.
   *
   * @return \SimpleXMLElement|null
   *   XML or NULL.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function apiCall(string $uri, array $options): ?\SimpleXMLElement {
    $uri = $this->apiSettings->get('base_url') . $uri;
    $options['form_params'] += [
      'strUserName' => $this->apiKeys['username'],
      'strPassword' => $this->apiKeys['password'],
      'strVersion' => $this->apiSettings->get('version'),
      // Deprecated key, but must be sent.
      'strKey1' => '',
    ];

    try {
      $response = $this->httpClient->request('POST', $uri, $options);
    }
    catch (\Exception $e) {
      watchdog_exception('haynespro', $e);
      return NULL;
    }

    $xml = new \SimpleXMLElement($response->getBody()->getContents());
    if ($this->xmlHasApiDefinedError($xml)) {
      $this->logApiDefinedError($xml);

      return NULL;
    }

    return $xml;
  }

  /**
   * Check if the API returns a custom defined error code and log it.
   *
   * @param \SimpleXMLElement $xml
   *   XML.
   *
   * @return bool
   *   TRUE if there is an error, else FALSE.
   */
  protected function xmlHasApiDefinedError(\SimpleXMLElement $xml): bool {
    if (isset($xml->DataArea->Error->Details) && $xml->DataArea->Error->Details->ErrorCode != self::STATUS_CODE_NO_VEHICLES_RETURNED) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Log API defined custom error.
   *
   * @param \SimpleXMLElement $xml
   *   XML.
   */
  protected function logApiDefinedError(\SimpleXMLElement $xml): void {
    $this->logger->error('API returned custom error code @code with message "@message".', [
      'code' => $xml->DataArea->Error->Details->ErrorCode,
      'message' => $xml->DataArea->Error->Details->ErrorDescription,
    ]);
  }

}
